# 工单打印

#### 介绍
将 Excel 数据表行按指定的 Word 模板批量生成 Word 文档并合并，方便打印。

#### 参考资料
https://www.cnblogs.com/zhenzaizai/p/7782748.html  
https://www.cnblogs.com/gaodaoheng/articles/10431853.html

#### 项目依赖
##### NPOI：
直接通过 NuGet 安装即可。

##### MS Office组件

word文档合并使用的是微软Microsoft.Office.Interop.Word，这个组件NuGet上只能在装有Office 2013的版本的电脑上运行，PASS掉。

因此，我们采用手动导入Microsoft.Office.Interop.Word.dll的方式。

需要注意的是，在COM组件添加 Microsoft Office [版本号].0 Object Library 这种方式好像也有问题。
我电脑上装的是 MS Office 2016，在COM组件看到的是 Microsoft Office 12.0 Object Library 和 Microsoft Office 12.0 Object Library，无论导入那个，运行程序都会报找不到 office 15 异常。

最终用的直接浏览定位到系统盘引用下面两个程序集解决。
C:\Windows\assembly\GAC_MSIL\Microsoft.Office.Interop.Word\15.0.0.0__71e9bce111e9429c\Microsoft.Office.Interop.Word.dll
C:\Windows\assembly\GAC_MSIL\office\15.0.0.0__71e9bce111e9429c\office.dll 
 
如果机器上没有安装MS Office 2007+ 的版本，直接在安装有MS Office 2007+的电脑上把这两个dll拷贝到过来应该也可以用（我在没有安装MS Office的电脑（装有WPS）上运行本项目发布的可执行文件是可以运行的）。

#### 编译运行/使用说明
1. 在 appsettings.json 文件配置软件环境变量，将 BaseFile 值配置为目标 Excel 文件所在路径。
2. F5或双击运行软件，按提示操作即可。
3. 转换成功后，会在当前工作目录下创建一个与目标 Excle 同名的文件夹，内部以数字命名的文件是与数据表行对应的单个文档，all.docx是按顺序合并后的文档。
4. word文档合并使用的是微软Microsoft.Office.Interop.Word，如果当前运行机器上没有安装 MS Office 2007+ 的版本，合并功能可能无法使用。

#### 目标Excel表格格式
1. 该工具读取的数据源只能是后缀名为 .xlsx 的文档，不支持 .xls 和其他格式的文件。
2. 该工具只会读取 Excle 工作表的第一个工作簿（Sheet）的数据。
3. 该工具默认会读取 Excle 表格的第一行数据作为表头，表头名称必须与 Word 模板文件中的占位符一致，不一致将会导致替换失败。
4. 该工具读取数据时会以依据最下方和最右方的有数据的单元格为边界，请删除表格下方和右方多余的备注信息。
5. 数据区域内有空值单元格会导致程序读取到该数据行时，读取到的数据长度和表头长度不一致，导致转换失败，请确保数据区域内没有空值单元格。

空值单元格(单元格本身为空或值为空)和空白单元格（单元格有空白或空格字符）在 Excel 内无法直观区分出来，如果遇到读取到的数据长度和表头长度不一致的错误，请在Excle表格中执行以下操作。

按 Ctrl + G 快捷键打开定位窗口，点击 定位条件 按钮，选择 “空值”，确定。
系统会选中全部值为空的单元格，这时直接在键盘上敲入几个空格键，并按 Ctrl + Enter 即可。
提示“未找到单元格”表示当前工作簿中没有空值单元格。

#### Word打印模板
1. 打印模板必须为 .docx 文档，不支持 .doc 和其他格式文件。
2. 打印模板内使用 表头名称 并用大括号括起来作为占位符（例：{对应Excel表头}），生成时将用实际值替换该占位符。
3. 模板内定义的定位符值(不含{})如果在数据表头中未找到对应的值，将不会备替换，所以注意保证数据表中有对应占位符列头。

实例：xlsx 表格中表头有，姓名、年龄、性别，那么可以在模板内使用“{姓名}”、“{年龄}”，“{性别}”作为占位符。

#### 运行效果
![运行效果](https://images.gitee.com/uploads/images/2019/1220/134406_8b1da758_376810.png "1.png")
![生成的docx文件](https://images.gitee.com/uploads/images/2019/1220/134425_bc6bc003_376810.png "2.png")
![生成的docx文件内容](https://images.gitee.com/uploads/images/2019/1220/134436_4d5d5403_376810.png "3.png")